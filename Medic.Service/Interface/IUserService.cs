﻿using Medic.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medic.Service.Interface
{
    public interface IUserService
    {
        #region User Group

        List<UserGroup> GetUserGroups();

        UserGroup GetUserGroup(int id);

        void AddUserGroup(UserGroup userGroup);

        void UpdateUserGroup(UserGroup userGroup);

        void DeleteUserGroup(int id);

        #endregion

        #region User

        User Authenticate(string loginName, string password);
        
        List<User> GetUsers(bool isActiveOnly);
        
        User GetUser(int id);
        
        void UpdateUser(User user);

        void AddUser(User user);
        
        void DeleteUser(int id);

        void ResetPassword(User user);

        #endregion
    }
}
