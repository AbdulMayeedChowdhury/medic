﻿using Medic.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medic.Service.Interface
{
    public interface IConfigurationService
    {
        #region Services

        List<Core.Entities.Service> GetServices(bool isActiveOnly);

        Core.Entities.Service GetService(int id);

        void AddService(Core.Entities.Service service);

        void UpdateService(Core.Entities.Service service);
        
        void DeleteService(int id);
        
        #endregion
    }
}
