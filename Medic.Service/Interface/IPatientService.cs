﻿using Medic.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medic.Service.Interface
{
    public interface IPatientService
    {
        List<Patient> GetPatients();

        Patient GetPatient(int id);

        void AddPatient(Patient patient);

        void UpdatePatient(Patient patient);

        void DeletePatient(int id);

        #region Patient Address

        List<PatientAddress> GetPatientAddresses(int patientId);

        List<PatientAddress> GetPatientAddresses(int patientId, AddressType addressType);

        PatientAddress GetPatientAddress(int patientId, int addressId);

        void AddAddress(PatientAddress newAddress);

        void UpdateAddress(PatientAddress address);

        void DeleteAddress(int addressId);

        #endregion

        #region Patient Case

        List<PatientCase> GetPatientCases(DateTime fromDate, DateTime toDate);

        List<PatientCase> GetPatientCases(CaseStatus caseStatus);

        List<PatientCase> GetPatientCases(int patientId);

        PatientCase GetPatientCase(int caseId);

        void CreatePatientCase(PatientCase newCase);

        void UpdatePatientCase(PatientCase updatedCase);

        void ChangePatientCaseStatus(PatientCase patinetCase, CaseStatus newStatus);
        
        void DeletePatientCase(int caseId);

        #endregion

        #region Case Visits

        List<PatientCaseVisit> GetPatientCaseVisits();

        PatientCaseVisit GetPatientCaseVisit(int visitId);

        void AddPatientCaseVisit(PatientCaseVisit newVisit);

        void UpdatePatientVisit(PatientCaseVisit updatedVisit);

        void DeletePatientVisit(int visitId);

        List<PatientCaseService> GetPatientCaseServices(int visitId);

        void AddServiceToPatientVisit(PatientCaseService newService);

        void RemoveServiceFromPatientVisit(int visitId, int serviceId, int serialNo);

        void UpdatePatientCaseService(PatientCaseService updatedService);

        #endregion

        #region Payments

        List<PatientCasePayment> GetCasePayments(int caseId);

        PatientCasePayment GetCasePayment(int paymentId);

        void AddPayment(PatientCasePayment newPayment);
        
        void UpdatePayment(PatientCasePayment updatedPayment);

        void DeletePayment(int paymentId);

        #endregion
    }
}
