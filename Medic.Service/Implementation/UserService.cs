﻿using Medic.Core.Entities;
using Medic.Core.Manager;
using Medic.Service.Interface;
using System;
using System.Collections.Generic;

namespace Medic.Service.Implementation
{
    public class UserService : IUserService
    {
        #region User Group

        public List<UserGroup> GetUserGroups()
        {
            return new UserManager().GetUserGroups();
        }

        public UserGroup GetUserGroup(int id)
        {
            return new UserManager().GetUserGroup(id);
        }

        public void AddUserGroup(UserGroup userGroup)
        {
            new UserManager().AddUserGroup(userGroup);
        }

        public void UpdateUserGroup(UserGroup userGroup)
        {
            new UserManager().UpdateUserGroup(userGroup);
        }

        public void DeleteUserGroup(int id)
        {
            new UserManager().DeleteUserGroup(id);
        }

        #endregion

        #region User

        public User Authenticate(string loginName, string password)
        {
            return new UserManager().Authenticate(loginName, password);
        }

        public List<User> GetUsers(bool isActiveOnly)
        {
            return new UserManager().GetUsers(isActiveOnly);
        }

        public User GetUser(int id)
        {
            return new UserManager().GetUser(id);
        }

        public void UpdateUser(User user)
        {
            new UserManager().UpdateUser(user);
        }

        public void AddUser(User user)
        {
            new UserManager().AddUser(user);
        }

        public void DeleteUser(int id)
        {
            new UserManager().DeleteUser(id);
        }

        public void ResetPassword(User user)
        {
            new UserManager().ResetPassword(user);
        }

        #endregion
    }
}
