﻿using Medic.Service.Interface;
using System;
using System.Collections.Generic;
using Medic.Core.Manager;

namespace Medic.Service.Implementation
{
    public class ConfigurationService : IConfigurationService
    {
        #region Service

        public List<Core.Entities.Service> GetServices(bool isActiveOnly)
        {
            return new ConfigurationManager().GetServices(isActiveOnly);
        }

        public Core.Entities.Service GetService(int id)
        {
            return new ConfigurationManager().GetService(id);
        }

        public void AddService(Core.Entities.Service service)
        {
            new ConfigurationManager().AddService(service);
        }

        public void UpdateService(Core.Entities.Service service)
        {
            new ConfigurationManager().UpdateService(service);
        }

        public void DeleteService(int id)
        {
            new ConfigurationManager().DeleteService(id);
        }

        #endregion
    }
}
