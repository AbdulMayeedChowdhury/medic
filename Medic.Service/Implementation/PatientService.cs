﻿using Medic.Core.Entities;
using Medic.Core.Manager;
using Medic.Service.Interface;
using System;
using System.Collections.Generic;

namespace Medic.Service.Implementation
{
    public class PatientService : IPatientService
    {
        #region Patient

        public List<Patient> GetPatients()
        {
            return new PatientManager().GetPatients();
        }

        public Patient GetPatient(int id)
        {
            return new PatientManager().GetPatient(id);
        }

        public void AddPatient(Patient patient)
        {
            new PatientManager().AddPatient(patient);
        }

        public void UpdatePatient(Patient patient)
        {
            new PatientManager().UpdatePatient(patient);
        }

        public void DeletePatient(int id)
        {
            new PatientManager().DeletePatient(id);
        }

        #endregion

        #region Patient Address

        public List<PatientAddress> GetPatientAddresses(int patientId)
        {
            return new PatientManager().GetPatientAddresses(patientId);
        }

        public List<PatientAddress> GetPatientAddresses(int patientId, AddressType addressType)
        {
            return new PatientManager().GetPatientAddresses(patientId, addressType);
        }

        public PatientAddress GetPatientAddress(int patientId, int addressId)
        {
            return new PatientManager().GetPatientAddress(patientId, addressId);
        }

        public void AddAddress(PatientAddress newAddress)
        {
            new PatientManager().AddAddress(newAddress);
        }

        public void UpdateAddress(PatientAddress address)
        {
            new PatientManager().UpdateAddress(address);
        }

        public void DeleteAddress(int addressId)
        {
            new PatientManager().DeleteAddress(addressId);
        }

        #endregion

        #region Patient Case

        public List<PatientCase> GetPatientCases(DateTime fromDate, DateTime toDate)
        {
            return new PatientManager().GetPatientCases(fromDate, toDate);
        }

        public List<PatientCase> GetPatientCases(CaseStatus caseStatus)
        {
            return new PatientManager().GetPatientCases(caseStatus);
        }

        public List<PatientCase> GetPatientCases(int patientId)
        {
            return new PatientManager().GetPatientCases(patientId);
        }

        public PatientCase GetPatientCase(int caseId)
        {
            return new PatientManager().GetPatientCase(caseId);
        }

        public void CreatePatientCase(PatientCase newCase)
        {
            new PatientManager().CreatePatientCase(newCase);
        }

        public void UpdatePatientCase(PatientCase updatedCase)
        {
            new PatientManager().UpdatePatientCase(updatedCase);
        }

        public void ChangePatientCaseStatus(PatientCase patinetCase, CaseStatus newStatus)
        {
            new PatientManager().ChangePatientCaseStatus(patinetCase, newStatus);
        }

        public void DeletePatientCase(int caseId)
        {
            new PatientManager().DeletePatientCase(caseId);
        }

        #endregion

        #region Case Visit

        public List<PatientCaseVisit> GetPatientCaseVisits()
        {
            return new PatientManager().GetPatientCaseVisits();
        }

        public PatientCaseVisit GetPatientCaseVisit(int visitId)
        {
            return new PatientManager().GetPatientCaseVisit(visitId);
        }

        public void AddPatientCaseVisit(PatientCaseVisit newVisit)
        {
            new PatientManager().AddPatientCaseVisit(newVisit);
        }

        public void UpdatePatientVisit(PatientCaseVisit updatedVisit)
        {
            new PatientManager().UpdatePatientVisit(updatedVisit);
        }

        public void DeletePatientVisit(int visitId)
        {
            new PatientManager().DeletePatientVisit(visitId);
        }

        #endregion

        #region Case Services

        public List<PatientCaseService> GetPatientCaseServices(int visitId)
        {
            return new PatientManager().GetPatientCaseServices(visitId);
        }

        public void AddServiceToPatientVisit(PatientCaseService newService)
        {
            new PatientManager().AddServiceToPatientVisit(newService);
        }

        public void RemoveServiceFromPatientVisit(int visitId, int serviceId, int serialNo)
        {
            new PatientManager().RemoveServiceFromPatientVisit(visitId, serviceId, serialNo);
        }

        public void UpdatePatientCaseService(PatientCaseService updatedService)
        {
            new PatientManager().UpdatePatientCaseService(updatedService);
        }

        #endregion

        #region Case Payment

        public List<PatientCasePayment> GetCasePayments(int caseId)
        {
            return new PatientManager().GetCasePayments(caseId);
        }

        public PatientCasePayment GetCasePayment(int paymentId)
        {
            return new PatientManager().GetCasePayment(paymentId);
        }

        public void AddPayment(PatientCasePayment newPayment)
        {
            new PatientManager().AddPayment(newPayment);
        }

        public void UpdatePayment(PatientCasePayment updatedPayment)
        {
            new PatientManager().UpdatePayment(updatedPayment);
        }

        public void DeletePayment(int paymentId)
        {
            new PatientManager().DeletePayment(paymentId);
        }

        #endregion
    }
}
