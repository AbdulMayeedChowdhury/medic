﻿
using Medic.Core.Manager;
using Medic.Core.Entities;
using Medic.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Medic.Web.Controllers
{
    public class LoginController : Controller
    {
        
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userManager = new UserManager();

                    User loginUser = userManager.Authenticate(loginViewModel.LoginName, loginViewModel.Password);

                    if (loginUser == null)
                    {
                        ModelState.AddModelError("Validation", "Invalid Login ID or Password.");
                        return View(loginViewModel);
                    }

                    FormsAuthentication.SetAuthCookie(loginViewModel.LoginName, false);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }

                    Session["UserId"] = loginUser.Id;
                    Session["LoginName"] = loginUser.LoginName;
                    Session["UserFullName"] = loginUser.FullName;

                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception exception)
            {
                ModelState.AddModelError("Error", exception.Message);
                return View(loginViewModel);
            }

            return View(loginViewModel);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            Session["UserId"] = null;
            Session["LoginName"] = null;
            Session["UserFullName"] = null;

            return RedirectToAction("Login", "Login");
        }
    }
}
