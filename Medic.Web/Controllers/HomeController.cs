﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medic.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            if(!IsValidLogin())
            {
                return RedirectToAction("Login", "Login");
            }

            return View();
        }

        private bool IsValidLogin()
        {
            var loginUserName = (string)Session["LoginName"];

            return !string.IsNullOrWhiteSpace(loginUserName);
        }
    }
}
