﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medic.Web.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Login name required"), DisplayName("Login Name")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Password required"), DataType(DataType.Password)]
        public string Password { get; set; }
    }
}