﻿using System;
using System.Collections.Generic;
using System.Linq;
using Medic.Core.Entities;

namespace Medic.Core.Manager
{
    public class ConfigurationManager
    {
        #region Services

        public List<Service> GetServices(bool isActiveOnly)
        {
            List<Service> services;

            using (var context = new MedicDbContext())
            {
                if (isActiveOnly)
                {
                    services = context.Services.Where(s => s.IsActive).ToList();
                }
                else
                {
                    services = context.Services.ToList();
                }
            }

            return services;
        }

        public Service GetService(int id)
        {
            Service service;

            using (var context = new MedicDbContext())
            {
                service = context.Services.SingleOrDefault(s => s.Id == id);
            }

            return service;
        }

        public void AddService(Service service)
        {
            var context = new MedicDbContext();

            context.Services.Add(service);

            context.SaveChanges();
        }

        public void UpdateService(Service service)
        {
            var context = new MedicDbContext();

            if (service.Id > 0)
            {
                var existingService = context.Services.SingleOrDefault(s => s.Id == service.Id);

                if (existingService != null)
                {
                    existingService.Name = service.Name;
                    existingService.Price = service.Price;
                    existingService.IsActive = service.IsActive;
                }
                else
                {
                    throw new Exception("Invalid service.");
                }

                context.SaveChanges();
            }
            else
            {
                throw new Exception("Invalid service.");
            }
        }

        public void DeleteService(int id)
        {
            using (var context = new MedicDbContext())
            {
                var service = context.Services.SingleOrDefault(s => s.Id == id);

                if (service != null)
                {
                    if (context.PatientCaseServices.Any(s => s.ServiceId == service.Id))
                    {
                        throw new Exception("This service is already in use.");
                    }

                    context.Services.Remove(service);

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid service.");
                }
            }
        }

        #endregion
    }
}
