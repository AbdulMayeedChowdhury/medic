﻿using System;
using System.Linq;
using Medic.Core.Entities;
using System.Collections.Generic;
using Medic.Core.Helper;

namespace Medic.Core.Manager
{
    public class UserManager
    {
        #region User Group Related Methods

        public List<UserGroup> GetUserGroups()
        {
            List<UserGroup> userGroups;

            using (var context = new MedicDbContext())
            {
                userGroups = context.UserGroups.ToList();
            }

            return userGroups;
        }

        public UserGroup GetUserGroup(int id)
        {
            UserGroup userGroup;

            using (var context = new MedicDbContext())
            {
                userGroup = context.UserGroups.SingleOrDefault(u => u.Id == id);
            }

            return userGroup;
        }

        public void AddUserGroup(UserGroup userGroup)
        {
            var context = new MedicDbContext();

            context.UserGroups.Add(userGroup);

            context.SaveChanges();
        }

        public void UpdateUserGroup(UserGroup userGroup)
        {
            var context = new MedicDbContext();

            if (userGroup.Id > 0)
            {
                var existingGroup = context.UserGroups.SingleOrDefault(u => u.Id == userGroup.Id);

                if (existingGroup != null)
                {
                    existingGroup.Name = userGroup.Name;
                }
                else
                {
                    throw new Exception("Invalid user group.");                    
                }

                context.SaveChanges();
            }
            else
            {
                throw new Exception("Invalid user group.");
            }
        }

        public void DeleteUserGroup(int id)
        {
            using (var context = new MedicDbContext())
            {
                var userGroup = context.UserGroups.SingleOrDefault(u => u.Id == id);

                if (userGroup != null)
                {
                    context.UserGroups.Remove(userGroup);

                    context.SaveChanges();
                }
            }
        }

        #endregion

        #region User Related Methods

        public User Authenticate(string loginName, string password)
        {
            User authenticateUser;

            string encryptedPassword = SecurityHelper.EncodeToSha1(password);

            using (var context = new MedicDbContext())
            {
                authenticateUser =
                    context.Users.SingleOrDefault(u => u.LoginName == loginName && u.Password == encryptedPassword && u.IsActive);
            }

            return authenticateUser;
        }

        public List<User> GetUsers(bool isActiveOnly)
        {
            List<User> users;

            using (var context = new MedicDbContext())
            {
                if (isActiveOnly)
                {
                    users = context.Users.Include("UserGroup").Where(u=>u.IsActive).ToList();
                }
                else
                {
                    users = context.Users.Include("UserGroup").ToList();
                }
            }

            return users;
        }

        public User GetUser(int id)
        {
            User user;

            using (var context = new MedicDbContext())
            {
                user = context.Users.Include("UserGroup").SingleOrDefault(u => u.Id == id);

            }

            return user;
        }

        public void UpdateUser(User user)
        {
            var context = new MedicDbContext();

            if (user.Id > 0)
            {
                var existingUser = context.Users.SingleOrDefault(u => u.Id == user.Id);

                if (existingUser != null)
                {
                    existingUser.UserGroupId = user.UserGroupId;
                    existingUser.FullName = user.FullName;
                    existingUser.IsActive = user.IsActive;
                }
            }
            else
            {
                throw new Exception("Invalid user.");
            }

            context.SaveChanges();
        }

        public void AddUser(User user)
        {
            var context = new MedicDbContext();

            user.IsActive = true;
            user.Password = SecurityHelper.EncodeToSha1(user.Password);

            context.Users.Add(user);
        }

        public void DeleteUser(int id)
        {
            using (var context = new MedicDbContext())
            {
                var user = context.Users.SingleOrDefault(u => u.Id == id);

                if (user != null)
                {
                    context.Users.Remove(user);

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid user.");
                }
            }
        }

        public void ResetPassword(User user)
        {
            var context = new MedicDbContext();

            if (user.Id > 0)
            {
                var existingUser = context.Users.SingleOrDefault(u => u.Id == user.Id);

                if (existingUser != null)
                {
                    //var tail = new Random().Next(99999, 9999999);

                    existingUser.Password = SecurityHelper.EncodeToSha1("Medic_1234#");

                    context.SaveChanges();
                }
            }
        }

        #endregion
    }
}
