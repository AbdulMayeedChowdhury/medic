﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Medic.Core.Entities;

namespace Medic.Core.Manager
{
    public class PatientManager
    {
        public List<Patient> GetPatients()
        {
            List<Patient> patients;

            using (var context = new MedicDbContext())
            {
                patients = context.Patients.ToList();
            }

            return patients;
        }

        public Patient GetPatient(int id)
        {
            Patient patient;

            using (var context = new MedicDbContext())
            {
                patient = context.Patients.Include("Address").SingleOrDefault(p => p.PatientId == id);
            }

            return patient;
        }

        public void AddPatient(Patient patient)
        {
            var context = new MedicDbContext();

            context.Patients.Add(patient);
        }

        public void UpdatePatient(Patient patient)
        {
            using (var context = new MedicDbContext())
            {
                var existingPatient = context.Patients.SingleOrDefault(u => u.PatientId == patient.PatientId);

                if (existingPatient != null)
                {
                    existingPatient.Title = patient.Title;
                    existingPatient.FirstName = patient.FirstName;
                    existingPatient.LastName = patient.LastName;
                    existingPatient.Age = patient.Age;
                    existingPatient.Sex = patient.Sex;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid patient.");
                }
            }
        }

        public void DeletePatient(int id)
        {
            using (var context = new MedicDbContext())
            {
                if (context.PatientCases.Any(c => c.PatientId == id))
                {
                    throw new Exception("There exist one or more cases against this patinet.");
                }

                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var patient = context.Patients.SingleOrDefault(u => u.PatientId == id);

                        if (patient != null)
                        {
                            var addresses = context.PatientAddresses.Where(a => a.PatientId == id).ToList();

                            if (addresses.Count > 0)
                            {
                                context.PatientAddresses.RemoveRange(addresses);
                            }



                            context.Patients.Remove(patient);

                            context.SaveChanges();

                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception("Invalid patient.");
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }
        }

        #region Patient Address

        public List<PatientAddress> GetPatientAddresses(int patientId)
        {
            List<PatientAddress> addresses;

            using (var context = new MedicDbContext())
            {
                addresses = context.PatientAddresses.Where(a => a.PatientId == patientId).ToList();
            }

            return addresses;
        }

        public List<PatientAddress> GetPatientAddresses(int patientId, AddressType addressType)
        {
            List<PatientAddress> addresses;

            using (var context = new MedicDbContext())
            {
                addresses = context.PatientAddresses.Where(a => a.PatientId == patientId && a.AddressType == addressType).ToList();
            }

            return addresses;
        }

        public PatientAddress GetPatientAddress(int patientId, int addressId)
        {
            PatientAddress address;

            using (var context = new MedicDbContext())
            {
                address = context.PatientAddresses.SingleOrDefault(a => a.AddressId == addressId && a.PatientId == patientId);
            }

            return address;
        }

        public void AddAddress(PatientAddress newAddress)
        {
            int patientId = newAddress.PatientId;

            var patient = GetPatient(patientId);

            var context = new MedicDbContext();

            if (patient != null)
            {
                var existingAddresses = context.PatientAddresses.Where(a => a.PatientId == patientId).ToList();

                var lastSerialNo = existingAddresses.Count > 0 ? existingAddresses.Max(a => a.SerialNo) : 0;

                newAddress.SerialNo = lastSerialNo + 1;

                context.PatientAddresses.Add(newAddress);

                context.SaveChanges();
            }
            else
            {
                throw new Exception("Invalid patient.");
            }
        }

        public void UpdateAddress(PatientAddress address)
        {
            using (var context = new MedicDbContext())
            {
                var existingAddress = context.PatientAddresses.SingleOrDefault(a => a.AddressId == address.AddressId && a.PatientId == address.PatientId);

                if (existingAddress != null)
                {

                    existingAddress.AddressLine1 = address.AddressLine1;
                    existingAddress.AddressLine2 = address.AddressLine2;
                    existingAddress.City = address.City;
                    existingAddress.Country = address.Country;
                    existingAddress.AddressType = address.AddressType;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid address id.");
                }
            }

        }

        public void DeleteAddress(int addressId)
        {
            using (var context = new MedicDbContext())
            {
                var address = context.PatientAddresses.SingleOrDefault(a => a.AddressId == addressId);

                if (address != null)
                {
                    context.PatientAddresses.Remove(address);

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid address id.");
                }
            }
        }

        #endregion

        #region Patient Case

        public List<PatientCase> GetPatientCases(DateTime fromDate, DateTime toDate)
        {
            List<PatientCase> cases;

            using (var context = new MedicDbContext())
            {
                cases = context.PatientCases
                                .Where(pc => EntityFunctions.TruncateTime(pc.StartDate) >= EntityFunctions.TruncateTime(fromDate)
                                            && EntityFunctions.TruncateTime(pc.StartDate) <= EntityFunctions.TruncateTime(toDate)).ToList();
            }

            return cases;
        }

        public List<PatientCase> GetPatientCases(CaseStatus caseStatus)
        {
            List<PatientCase> cases;

            using (var context = new MedicDbContext())
            {
                cases = context.PatientCases
                                .Where(pc => pc.Status == caseStatus).ToList();
            }

            return cases;
        }

        public List<PatientCase> GetPatientCases(int patientId)
        {
            List<PatientCase> cases;

            using (var context = new MedicDbContext())
            {
                cases = context.PatientCases
                                .Include("PatientCasePayment")
                                .Include("PatientCaseVisit")
                                .Where(pc => pc.PatientId == patientId).ToList();
            }

            return cases;
        }

        public PatientCase GetPatientCase(int caseId)
        {
            PatientCase patientCase;

            using (var context = new MedicDbContext())
            {
                patientCase = context.PatientCases
                                    .Include("PatientCasePayment")
                                    .Include("PatientCaseVisit")
                                    .SingleOrDefault(c => c.CaseId == caseId);
            }

            return patientCase;
        }

        public void CreatePatientCase(PatientCase newCase)
        {
            int patientId = newCase.PatientId;

            var patient = GetPatient(patientId);

            var context = new MedicDbContext();

            if (patient != null)
            {
                newCase.CreateDate = DateTime.Now;
                context.PatientCases.Add(newCase);

                context.SaveChanges();
            }
            else
            {
                throw new Exception("Invalid patient.");
            }
        }

        public void UpdatePatientCase(PatientCase updatedCase)
        {
            using (var context = new MedicDbContext())
            {
                var existingCase = context.PatientCases.SingleOrDefault(c => c.CaseId == updatedCase.CaseId);

                if (existingCase != null)
                {

                    existingCase.CaseSummary = updatedCase.CaseSummary;
                    existingCase.CaseVisits = updatedCase.CaseVisits;
                    existingCase.StartDate = updatedCase.StartDate;
                    existingCase.EndDate = updatedCase.EndDate;
                    existingCase.Status = updatedCase.Status;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid address id.");
                }
            }
        }

        public void ChangePatientCaseStatus(PatientCase patinetCase, CaseStatus newStatus)
        {
            using (var context = new MedicDbContext())
            {
                var existingCase = context.PatientCases.SingleOrDefault(c => c.CaseId == patinetCase.CaseId);

                if (existingCase != null)
                {
                    existingCase.Status = newStatus;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid address id.");
                }
            }
        }

        public void DeletePatientCase(int caseId)
        {
            using (var context = new MedicDbContext())
            {
                if (context.PatientCasePayments.Any(c => c.CaseId == caseId))
                {
                    throw new Exception("There exist one or more payments against this case. Delete all payments and visits first.");
                }

                if (context.PatientCaseVisits.Any(c => c.CaseId == caseId))
                {
                    throw new Exception("There exist one or more visits against this case. Delete all payments and visits first.");
                }

                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var patientCase = context.PatientCases.SingleOrDefault(u => u.CaseId == caseId);

                        if (patientCase != null)
                        {
                            context.PatientCases.Remove(patientCase);

                            context.SaveChanges();

                            transaction.Commit();
                        }
                        else
                        {
                            throw new Exception("Invalid case.");
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }
        }

        #endregion

        #region Case Visits

        public List<PatientCaseVisit> GetPatientCaseVisits()
        {
            List<PatientCaseVisit> visits;

            using (var context = new MedicDbContext())
            {
                visits = context.PatientCaseVisits.Include("Service").ToList();
            }

            return visits;
        }

        public PatientCaseVisit GetPatientCaseVisit(int visitId)
        {
            PatientCaseVisit visit;

            using (var context = new MedicDbContext())
            {
                visit = context.PatientCaseVisits.Include("Service").SingleOrDefault(v => v.Id == visitId);
            }

            return visit;
        }

        public void AddPatientCaseVisit(PatientCaseVisit newVisit)
        {
            int caseId = newVisit.CaseId;

            var patientCase = GetPatientCase(caseId);

            var context = new MedicDbContext();

            if (patientCase != null)
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        context.PatientCaseVisits.Add(newVisit);

                        context.SaveChanges();

                        if (newVisit.Services.Any())
                        {
                            foreach (var caseService in newVisit.Services)
                            {
                                caseService.VisitId = newVisit.Id;
                            }

                            context.PatientCaseServices.AddRange(newVisit.Services);

                            context.SaveChanges();
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }

                }
            }
            else
            {
                throw new Exception("Invalid case.");
            }
        }

        public void UpdatePatientVisit(PatientCaseVisit updatedVisit)
        {
            using (var context = new MedicDbContext())
            {
                var existingVisit = context.PatientCaseVisits.SingleOrDefault(c => c.Id == updatedVisit.Id);

                if (existingVisit != null)
                {
                    existingVisit.VisitDate = updatedVisit.VisitDate;
                    existingVisit.VisitNote = updatedVisit.VisitNote;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid visit id.");
                }
            }
        }

        public void DeletePatientVisit(int visitId)
        {
            var existingVisit = GetPatientCaseVisit(visitId);

            var context = new MedicDbContext();

            if (existingVisit != null)
            {
                using (DbContextTransaction transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var services = context.PatientCaseServices.Where(s => s.VisitId == visitId).ToList();

                        if (services.Count > 0)
                        {
                            context.PatientCaseServices.RemoveRange(services);
                        }

                        context.PatientCaseVisits.Remove(existingVisit);

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();

                        throw new Exception("Failed to delete the visit.");
                    }

                }
            }
        }

        public List<PatientCaseService> GetPatientCaseServices(int visitId)
        {
            List<PatientCaseService> services;

            using (var context = new MedicDbContext())
            {
                services = context.PatientCaseServices.Include("Service").Where(s => s.VisitId == visitId).ToList();
            }

            return services;
        }

        private PatientCaseService GetPatientCaseService(int visitId, int serviceId, int serialNo)
        {
            PatientCaseService caseService;

            using (var context = new MedicDbContext())
            {
                caseService = context.PatientCaseServices.Include("Service")
                        .SingleOrDefault(s => s.VisitId == visitId && s.ServiceId == serviceId && s.SerialNo == serialNo);
            }

            return caseService;
        }

        public void AddServiceToPatientVisit(PatientCaseService newService)
        {
            var existingVisit = GetPatientCaseVisit(newService.VisitId);

            if (existingVisit != null)
            {
                var context = new MedicDbContext();

                var services = GetPatientCaseServices(newService.VisitId);

                newService.SerialNo = services.Count == 0 ? 1 : services.Max(s => s.SerialNo) + 1;

                context.PatientCaseServices.Add(newService);

                context.SaveChanges();
            }
            else
            {
                throw new Exception("Invalid visit.");
            }

        }

        public void RemoveServiceFromPatientVisit(int visitId, int serviceId, int serialNo)
        {
            var existingVisit = GetPatientCaseVisit(visitId);

            if (existingVisit != null)
            {
                var context = new MedicDbContext();

                var existingService = GetPatientCaseService(visitId, serviceId, serialNo);

                if (existingService != null)
                {
                    context.PatientCaseServices.Remove(existingService);

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid service.");
                }
            }
            else
            {
                throw new Exception("Invalid visit.");
            }
        }

        public void UpdatePatientCaseService(PatientCaseService updatedService)
        {
            using (var context = new MedicDbContext())
            {
                var existingService = context.PatientCaseServices.SingleOrDefault(s => s.Id == updatedService.Id);

                if (existingService != null)
                {
                    existingService.Price = updatedService.Price;
                    existingService.Quantity = updatedService.Quantity;
                    existingService.Amount = updatedService.Amount;

                    context.SaveChanges();
                }
            }

        }

        #endregion

        #region Payments

        public List<PatientCasePayment> GetCasePayments(int caseId)
        {
            var payments = new List<PatientCasePayment>();

            var existingCase = GetPatientCase(caseId);

            if (existingCase != null)
            {
                using (var context = new MedicDbContext())
                {
                    payments = context.PatientCasePayments.Where(p => p.CaseId == caseId).ToList();
                }

                return payments;
            }
            else
            {
                throw  new Exception("Invalid Case");
            }
        }

        public PatientCasePayment GetCasePayment(int paymentId)
        {
            PatientCasePayment payment;

            using (var context = new MedicDbContext())
            {
                payment = context.PatientCasePayments.SingleOrDefault(p => p.PaymentId == paymentId);
            }

            return payment;
        }

        public void AddPayment(PatientCasePayment newPayment)
        {
            var existingCase = GetCasePayment(newPayment.CaseId);

            if (existingCase != null)
            {
                using (var context = new MedicDbContext())
                {
                    context.PatientCasePayments.Add(newPayment);
                }
            }
            else
            {
                throw new Exception("Invalid Case");
            }
        }

        public void UpdatePayment(PatientCasePayment updatedPayment)
        {
            using (var context = new MedicDbContext())
            {
                var existingPayment = context.PatientCasePayments.SingleOrDefault(c=>c.PaymentId == updatedPayment.PaymentId);

                if (existingPayment != null)
                {
                    existingPayment.PaymentDate = updatedPayment.PaymentDate;
                    existingPayment.Amount = updatedPayment.Amount;

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid payment");
                }
            }
            
        }

        public void DeletePayment(int paymentId)
        {
            using (var context = new MedicDbContext())
            {
                var existingPayment = context.PatientCasePayments.SingleOrDefault(c => c.PaymentId == paymentId);

                if (existingPayment != null)
                {
                    context.PatientCasePayments.Remove(existingPayment);

                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("Invalid payment");
                }
            }
        }

        #endregion
    }
}
