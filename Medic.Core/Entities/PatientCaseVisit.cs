﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public class PatientCaseVisit
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int CaseId { get; set; }

        public DateTime VisitDate { get; set; }

        public string VisitNote { get; set; }

        public List<PatientCaseService> Services { get; set; }
            
        [ForeignKey("CaseId")]
        public PatientCase PatientCase { get; set; }
    }
}
