﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public class PatientCasePayment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaymentId { get; set; }

        public DateTime PaymentDate { get; set; }

        public int CaseId { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("CaseId")]
        public PatientCase PatientCase { get; set; }
    }
}
