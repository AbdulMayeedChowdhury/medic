﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public int UserGroupId { get; set; }

        [Required, MaxLength(30)]
        public string LoginName { get; set; }

        [Required, MaxLength(100)]
        public string FullName { get; set; }

        [MaxLength(50), Index("IX_EmailId", IsUnique = true)]
        public string EmailId { get; set; }

        [Required, MaxLength(500)]
        public string Password { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [ForeignKey("UserGroupId")]
        public UserGroup UserGroup { get; set; }
    }
}
