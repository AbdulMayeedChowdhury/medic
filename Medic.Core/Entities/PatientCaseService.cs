﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public class PatientCaseService
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int VisitId { get; set; }

        public int SerialNo { get; set; }

        public int ServiceId { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal Amount { get; set; }

        [ForeignKey("VisitId")]
        public PatientCaseVisit PatientCaseVisit { get; set; }

        [ForeignKey("ServiceId")]
        public Service Service { get; set; }
    }
}
