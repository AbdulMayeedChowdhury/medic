﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public class PatientCase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CaseId { get; set; }

        public int PatientId { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string CaseSummary { get; set; }

        public List<PatientCasePayment> Payments { get; set; }

        public List<PatientCaseVisit> CaseVisits { get; set; }

        public CaseStatus Status { get; set; }
            
        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
    }
}
