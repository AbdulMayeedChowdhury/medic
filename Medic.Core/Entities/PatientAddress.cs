﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medic.Core.Entities
{
    public enum AddressType
    {
        PresentAddress=1, ParmanentAddress=2, MailingAddress=3, BusinessAddress=4
    }

    public class PatientAddress
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AddressId { get; set; }

        public int PatientId { get; set; }

        public AddressType AddressType { get; set; }

        public int SerialNo { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public int PostCode { get; set; }

        public string Country { get; set; }

        [ForeignKey("PatientId")]
        public Patient Patient { get; set; }
    }

    
}
