﻿using System.Data.Entity;
using Medic.Core.Entities;

namespace Medic.Core
{
    public class MedicDbContext : DbContext
    {
        public MedicDbContext() : base("MedicDbConnectionString")
        {
            Database.SetInitializer<MedicDbContext>(new CreateDatabaseIfNotExists<MedicDbContext>());
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<UserGroup> UserGroups { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Service> Services { get; set; }

        public DbSet<Patient> Patients { get; set; }

        public DbSet<PatientAddress> PatientAddresses { get; set; }

        public DbSet<PatientCase> PatientCases { get; set; }

        public DbSet<PatientCasePayment> PatientCasePayments { get; set; }

        public DbSet<PatientCaseVisit> PatientCaseVisits { get; set; }

        public DbSet<PatientCaseService> PatientCaseServices { get; set; }

    }
}
