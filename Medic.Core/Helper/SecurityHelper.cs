﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Medic.Core.Helper
{
    public class SecurityHelper
    {
        public static string EncodeToSha1(string textToEncode)
        {
            var sha256Managed = new SHA256Managed();
            var hash = new StringBuilder();

            byte[] encryptedBytes = sha256Managed.ComputeHash(Encoding.UTF8.GetBytes(textToEncode), 0,
                Encoding.UTF8.GetByteCount(textToEncode));

            foreach (var encryptedByte in encryptedBytes)
            {
                hash.Append(encryptedByte.ToString("x2"));
            }

            return hash.ToString();
        }
    }
}
