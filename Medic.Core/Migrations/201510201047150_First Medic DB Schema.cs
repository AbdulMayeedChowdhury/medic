namespace Medic.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMedicDBSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientAddresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        PatientId = c.Int(nullable: false),
                        AddressType = c.Int(nullable: false),
                        SerialNo = c.Int(nullable: false),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        PostCode = c.Int(nullable: false),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        PatientId = c.Int(nullable: false, identity: true),
                        Title = c.Int(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Age = c.Int(nullable: false),
                        Sex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PatientId);
            
            CreateTable(
                "dbo.PatientCasePayments",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        PaymentDate = c.DateTime(nullable: false),
                        CaseId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.PatientCases", t => t.CaseId, cascadeDelete: true)
                .Index(t => t.CaseId);
            
            CreateTable(
                "dbo.PatientCases",
                c => new
                    {
                        CaseId = c.Int(nullable: false, identity: true),
                        PatientId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        CaseSummary = c.String(),
                    })
                .PrimaryKey(t => t.CaseId)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientCasePayments", "CaseId", "dbo.PatientCases");
            DropForeignKey("dbo.PatientCases", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.PatientAddresses", "PatientId", "dbo.Patients");
            DropIndex("dbo.PatientCases", new[] { "PatientId" });
            DropIndex("dbo.PatientCasePayments", new[] { "CaseId" });
            DropIndex("dbo.PatientAddresses", new[] { "PatientId" });
            DropTable("dbo.PatientCases");
            DropTable("dbo.PatientCasePayments");
            DropTable("dbo.Patients");
            DropTable("dbo.PatientAddresses");
        }
    }
}
