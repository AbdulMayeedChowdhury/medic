namespace Medic.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCaseStatusforPatientCase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PatientCases", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PatientCases", "Status");
        }
    }
}
