using System.Linq;
using Medic.Core.Entities;

namespace Medic.Core.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Medic.Core.MedicDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Medic.Core.MedicDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.UserGroups.AddOrUpdate(
                g => g.Name,
                new UserGroup { Name = "Administrator" }
                );

            context.SaveChanges();

            int groupId = context.UserGroups.First().Id;
            context.Users.AddOrUpdate(
                u => u.LoginName,
                new User { UserGroupId = groupId, LoginName = "Admin", FullName = "Admin", Password = "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb", IsActive = true });

        }
    }
}
