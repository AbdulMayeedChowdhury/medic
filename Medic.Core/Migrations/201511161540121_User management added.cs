namespace Medic.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Usermanagementadded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserGroupId = c.Int(nullable: false),
                        LoginName = c.String(nullable: false, maxLength: 30),
                        FullName = c.String(nullable: false, maxLength: 100),
                        EmailId = c.String(maxLength: 50),
                        Password = c.String(nullable: false, maxLength: 500),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserGroups", t => t.UserGroupId, cascadeDelete: true)
                .Index(t => t.UserGroupId)
                .Index(t => t.EmailId, unique: true);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "UserGroupId", "dbo.UserGroups");
            DropIndex("dbo.Users", new[] { "EmailId" });
            DropIndex("dbo.Users", new[] { "UserGroupId" });
            DropTable("dbo.Users");
            DropTable("dbo.UserGroups");
        }
    }
}
