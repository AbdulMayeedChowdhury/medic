namespace Medic.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addservicesandchangecasedetailstocasevisit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PatientCaseVisits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CaseId = c.Int(nullable: false),
                        VisitDate = c.DateTime(nullable: false),
                        VisitNote = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientCases", t => t.CaseId, cascadeDelete: true)
                .Index(t => t.CaseId);
            
            CreateTable(
                "dbo.PatientCaseServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VisitId = c.Int(nullable: false),
                        SerialNo = c.Int(nullable: false),
                        ServiceId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PatientCaseVisits", t => t.VisitId, cascadeDelete: true)
                .ForeignKey("dbo.Services", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.VisitId)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PatientCaseServices", "ServiceId", "dbo.Services");
            DropForeignKey("dbo.PatientCaseServices", "VisitId", "dbo.PatientCaseVisits");
            DropForeignKey("dbo.PatientCaseVisits", "CaseId", "dbo.PatientCases");
            DropIndex("dbo.PatientCaseServices", new[] { "ServiceId" });
            DropIndex("dbo.PatientCaseServices", new[] { "VisitId" });
            DropIndex("dbo.PatientCaseVisits", new[] { "CaseId" });
            DropTable("dbo.Services");
            DropTable("dbo.PatientCaseServices");
            DropTable("dbo.PatientCaseVisits");
        }
    }
}
